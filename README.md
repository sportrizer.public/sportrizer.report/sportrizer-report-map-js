# SportRIZER.report map client library

Javascript library that displays the SportySKY map on the frontend

![](./preview.png)

## Usage

### Map creation

---

```javascript
import Map from '@sportrizer/report-map-js';

const container = docuement.querySelectorAll('#map-container');
const map = new Map(container, {
  apiEndpoint: 'http://mywebsite/map-api',
});
```

**Parameters :**

- `domElement` : The DOM Element you want to put the map in.

- `config` : an object for the map configuration containing these attributes :

  - `apiEndpoint` : _string_  
    an authenticated proxy to the sportrizer.report api.  
    **default :** `/api/`
  - `iconSize` : _integer_  
    the spots icon size in pixel.  
    **default :** `40`
  - `iconSizeTiny` : _integer_  
    the spots icon size in pixel on small devices.  
    **default :** `20`
  - `iconTinyBefore`: _integer_  
    the breakpoint in pixel for switching between tiny icons and normal icons.  
    **default : 1024**
  - `textWeather`: _function(airTemperature)_  
    **default** : `` airTemperature => `${airTemperature} °c` ``
  - `textWind`: _function(windSpeed)_  
    **default**: `` windSpeed => `${windSpeed} Km/h` ``
  - `textWindGust`: _function(windGust)_  
    **default**: `` airTemperature => `<br><em>(Max. ${windGust})</em>` ``
  - `hoverSpot`  
    **default**: `null`
  - `clickSpot`  
    **default**: `null`
  - `hasAqData`  
    **default**: `null`

Example :

```javascript
{
  apiEndpoint: 'http://mywebsite/map-api',
  iconSize: 42,
  textWind: windSpeed => `${windSpeed * 0.539957} nd`
}
```

### Methods

---

#### displayCountry

Example :

```javascript
map.displayCountry(new Date(), 'FR', function(spots) {
  console.log(spots);
});
```

**Parameters**

- `date` : a `Date` object that will be use to return corresponding forecast data
- `displayIsoCode` : a `string` of the region [ISO Code](https://en.wikipedia.org/wiki/ISO_3166-1)
- `callback` : a callback `function` that will be called after loading forecast data of the spots giving one argument :
  - `spots` : the spots forecast data returned

#### displayRegion

Example :

```javascript
map.displayRegion(new Date(), 'FR-BRE', function(spots) {
  console.log(spots);
});
```

**Parameters**

- `date` : a `Date` object that will be use to return corresponding forecast data
- `regionIsoCode` : a `string` of the region [ISO Code](https://en.wikipedia.org/wiki/ISO_3166-2)
- `callback` : a callback `function` that will be called after loading forecast data of the spots giving one argument :

  - `spots` : the spots forecast data returned

#### displayDepartment

Example :

```javascript
map.displayDepartment(new Date(), 'FR-29', function(spots) {
  console.log(spots);
});
```

**Parameters**

- `date` : a `Date` object that will be use to return corresponding forecast data
- `regionIsoCode` : a `string` of the department [ISO Code](https://en.wikipedia.org/wiki/ISO_3166-2)
- `callback` : a callback `function` that will be called after loading forecast data of the spots giving one argument :
  - `spots` : the spots forecast data returned

#### getSpotForecast

Example :

```javascript
map.getSpotForecast(
    new Date("2020-04-22")),
    new Date("2020-04-29"),
    '64678710-3c65-11ea-94b8-0242ac120008',
    function(spot, mapView) {
        console.log(spot);
    }
);
```

**Parameters**

- `startDate` : a `Date` object that will be use to return corresponding forecast data
- `endDate` : a `Date` object that will be use to return corresponding forecast data
- `spotUuid` : a `string` of the spot id previously returned by the API
- `callback` : a callback `function` that will be called after loading forecast data of the spot giving two arguments :
  - `spot` : the spot forecast data returned
  - `mapView` : the current view used ( `department` , `region` or `country` )

#### showWind

Example :

```javascript
map.showWind();
```

#### showWeather

Example :

```javascript
map.showWeather();
```

## Integration with a server client

The SportySKY API needs your server to be authenticated. Therefore, you must implement a server client in order to retrieve data.

Implementations :

- [PHP client](https://github.com/SportRIZER/sportysky-client-php#integration-with-the-sportysky-javascript-library)
