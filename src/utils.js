import { isNumber } from 'lodash';

export function knot2kmh(knots) {
  if (!isNumber(knots) || knots < 0) throw new Error('Knots parameter must be a positive number');

  return Math.round(1.852001 * knots);
}

export function padLeft(number, base, chr) {
  const length = String(base || 10).length - String(number).length + 1;
  return length > 0 ? new Array(length).join(chr || '0') + number : number;
}

export function formatDate(date) {
  return `${date.getFullYear()}-${padLeft(date.getMonth() + 1)}-${padLeft(
    date.getDate(),
  )}T${padLeft(date.getHours())}:${padLeft(date.getMinutes())}:00+00:00`;
}

export function formatParams(params) {
  return Object.keys(params)
    .map(key => {
      return `${key}=${encodeURIComponent(params[key])}`;
    })
    .join('&');
}
