import { knot2kmh } from './utils';

describe('knot to kmh conversion', () => {
  it('throws an error with invalid types', () => {
    expect(() => knot2kmh('35')).toThrowError();
  });
  it('throws an error with negative values', () => {
    expect(() => knot2kmh(-2)).toThrowError();
  });
  it('returns 0 kmh for 0 knot', () => {
    expect(knot2kmh(0)).toBe(0);
  });
  it('returns correct values', () => {
    expect(knot2kmh(16)).toBe(30);
    expect(knot2kmh(15.8)).toBe(29);
  });
});
