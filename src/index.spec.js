import { Map, createMapInstance } from '.';

describe('Map class', () => {
  it('exists', () => {
    expect(Map).toBeDefined();
  });
});

describe('instanciator function', () => {
  it('exists', () => {
    expect(createMapInstance).toBeDefined();
  });
});
