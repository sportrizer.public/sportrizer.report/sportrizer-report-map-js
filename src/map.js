import { get, merge, defaultTo } from 'lodash';
import { formatDate, formatParams, knot2kmh } from './utils';

export const DEPARTMENT_VIEW = 'department';
export const REGION_VIEW = 'region';
export const COUNTRY_VIEW = 'country';
export const DIFF_WIND_GUST = 18;
export class Map {
  constructor(domElement, config) {
    if (!domElement) return;

    this.domElement = domElement;
    this.domElement.classList.add('sportysky_map');
    this.mapConfig = merge(
      {
        apiEndpoint: '/api/',
        iconSize: 40,
        iconSizeTiny: 20,
        iconTinyBefore: 1024,
        textWeather: airTemperature => `${airTemperature} °c`,
        textWind: windSpeed => `${windSpeed} Km/h`,
        textWindGust: windGust => `<em>(Max. ${windGust})</em>`,
        hoverSpot: null,
        clickSpot: null,
        hasAqData: () => {},
      },
      config,
    );
    this.api = this.mapConfig.apiEndpoint;

    this.init();
  }

  init() {
    if (this.domElement.querySelectorAll('.sportysky_backmap').length === 0) {
      this.canvas = document.createElement('canvas');
      this.domElement.append(this.canvas);
      this.context = this.canvas.getContext('2d');
      this.canvas.style.width = '100%';
      this.canvas.classList.add('sportysky_backmap');
      this.canvas.width = this.domElement.offsetWidth;
      this.canvas.style.width = `${this.canvas.width}px`;
    } else {
      this.canvas.style.width = '100%';
      this.canvas.width = this.domElement.offsetWidth;
      this.canvas.style.width = `${this.canvas.width}px`;
    }
  }

  draw(data, mapView, callback) {
    const mapUrl = get(data, `[${mapView}Map].file.contentUrl`);

    if (!mapUrl) return;

    const imageMap = new Image();
    imageMap.src = mapUrl;
    const map = this;

    // eslint-disable-next-line func-names
    imageMap.onload = function() {
      map.canvas.height = this.height * (map.canvas.offsetWidth / this.width);
      map.canvas.style.height = `${map.canvas.height}px`;
      map.context.drawImage(imageMap, 0, 0, map.canvas.width, map.canvas.height);
      map.initWrapperSpots();
      if (typeof callback === 'function') {
        callback();
      }
    };
  }

  initSpots(data, mapView) {
    this.spots = [];
    data.spots.forEach(spotData => {
      if (spotData.feeds.length < 1) {
        return;
      }
      const spot = {
        ...spotData,
      };

      spot.mapPosition = spotData[`${mapView}MapPosition`];

      if (window.innerWidth <= this.mapConfig.iconTinyBefore) {
        spot.iconSize = {
          width: this.mapConfig.iconSizeTiny,
          height: this.mapConfig.iconSizeTiny,
        };
      } else {
        spot.iconSize = {
          width: this.mapConfig.iconSize,
          height: this.mapConfig.iconSize,
        };
      }

      spot.topLeft = {
        x: 0.01 * spot.mapPosition.xAxis * this.canvas.width + spot.iconSize.width * 0.5,
        y: 0.01 * spot.mapPosition.yAxis * this.canvas.height + spot.iconSize.height * 0.5,
      };

      this.spots.push(spot);
    });
  }

  initWrapperSpots() {
    if (this.domElement.querySelectorAll('.sportysky_wrapperspots').length === 0) {
      this.wrapperspots = document.createElement('div');
      this.wrapperspots.classList.add('sportysky_wrapperspots');
      this.wrapperspots.width = this.canvas.width;
      this.wrapperspots.height = this.canvas.height;
      this.wrapperspots.style.width = `${this.canvas.width}px`;
      this.wrapperspots.style.height = `${this.canvas.height}px`;
      this.domElement.append(this.wrapperspots);
    } else {
      this.wrapperspots.width = this.canvas.width;
      this.wrapperspots.height = this.canvas.height;
      this.wrapperspots.style.width = `${this.canvas.width}px`;
      this.wrapperspots.style.height = `${this.canvas.height}px`;
    }
  }

  cleanWrapperSpots() {
    const wrapper = this.domElement.querySelectorAll('.sportysky_wrapperspots')[0];
    if (wrapper.querySelectorAll('.sportysky_spots').length > 0) {
      wrapper.innerHTML = '';
    }
  }

  displayCurrentType() {
    if (this.domElement.querySelectorAll('.sportysky_wrapperspots').length === 0) {
      this.initWrapperSpots();
    } else {
      this.cleanWrapperSpots();
    }
    switch (this.currentWeatherType) {
      case 'aq':
        this.displayAqIcons();
        break;
      case 'weather':
        this.displayWeatherIcons();
        break;
      case 'wind':
        this.displayWindIcons();
        break;
      default:
        this.currentWeatherType = 'weather';
        this.displayWeatherIcons();
        break;
    }
  }

  displaySpotIcon(source, rotation, spot, type, callback) {
    const pictoCoordinates = {
      x: spot.topLeft.x,
      y: spot.topLeft.y,
    };

    const spotDiv = document.createElement('div');
    const spotImg = document.createElement('img');
    const spotText = document.createElement('p');
    spotDiv.classList.add('sportysky_spots');
    spotDiv.style.left = `${pictoCoordinates.x - spot.iconSize.width}px`;
    spotDiv.style.top = `${pictoCoordinates.y - spot.iconSize.height}px`;

    // eslint-disable-next-line unicorn/prevent-abbreviations
    spotImg.src = source;
    spotImg.width = spot.iconSize.width;
    spotImg.height = spot.iconSize.height;
    if (type === 'wind') {
      spotImg.classList.add(spot.feeds[0].windDirectionString.toLowerCase());
      const wind = knot2kmh(defaultTo(get(spot, 'feeds[0].windSpeed', 0), 0));
      const gust = knot2kmh(defaultTo(get(spot, 'feeds[0].windGust', 0), 0));

      spotText.innerHTML = this.mapConfig.textWind(wind);

      if (gust >= wind + DIFF_WIND_GUST) {
        spotText.innerHTML = this.mapConfig.textWindGust(gust);
      } else if (wind === 0) {
        spotText.innerHTML = '';
        spotImg.src = '';
      }
    } else if (type === 'aq') {
      spotText.innerHTML = '';
    } else {
      spotText.innerHTML = this.mapConfig.textWeather(spot.feeds[0].airTemperature);
    }

    if (spotImg.src !== '') spotDiv.append(spotImg);
    if (spotText.innerHTML !== '') spotDiv.append(spotText);

    if (typeof this.mapConfig.hoverSpot === 'function') {
      spotDiv.addEventListener('mouseenter', event => {
        this.mapConfig.hoverSpot(spot, event);
      });
      spotDiv.addEventListener('mousemove', event => {
        this.mapConfig.hoverSpot(spot, event);
      });
      spotDiv.addEventListener('mouseleave', event => {
        this.mapConfig.hoverSpot(spot, event);
      });
    }

    if (typeof this.mapConfig.clickSpot === 'function') {
      spotDiv.addEventListener('click', event => {
        this.mapConfig.clickSpot(spot, event);
      });
      spotDiv.addEventListener('touchend', event => {
        this.mapConfig.clickSpot(spot, event);
      });
    }
    this.wrapperspots.append(spotDiv);

    if (typeof callback === 'function') {
      callback(spot);
    }
  }

  displayWeatherIcons() {
    this.spots.forEach(spot => {
      if (spot.feeds.length === 0) {
        return;
      }
      const weatherIconSource = spot.feeds[0].icon;
      this.displaySpotIcon(weatherIconSource, 0, spot, 'weather');
    });
  }

  displayWindIcons() {
    this.spots.forEach(spot => {
      const windIconSource = spot.feeds[0].windIcon;
      this.displaySpotIcon(
        windIconSource,
        (spot.feeds[0].windDirection * Math.PI) / 180,
        spot,
        'wind',
      );
    });
  }

  displayAqIcons() {
    this.spots.forEach(spot => {
      if (spot.feeds.length === 0) {
        return;
      }
      const weatherIconSource = spot.feeds[0].airQualityIcon;
      if (weatherIconSource != null) {
        this.displaySpotIcon(weatherIconSource, 0, spot, 'aq');
        this.mapConfig.hasAqData(false);
      } else {
        this.mapConfig.hasAqData(true);
      }
    });
  }

  showWind() {
    this.currentWeatherType = 'wind';
    this.draw(this.lastData, this.mapView, () => {
      this.displayCurrentType();
    });
  }

  showWeather() {
    this.currentWeatherType = 'weather';
    this.draw(this.lastData, this.mapView, () => {
      this.displayCurrentType();
    });
  }

  showAQ() {
    this.currentWeatherType = 'aq';
    this.draw(this.lastData, this.mapView, () => {
      this.displayCurrentType();
    });
  }

  loadDataCallback(data, mapView) {
    this.draw(data, mapView, () => {
      this.initSpots(data, mapView);
      this.displayCurrentType();
    });
  }

  displayCountry(date, countryIsoCode, callback) {
    this.spots = [];
    this.loadData(date, null, null, null, countryIsoCode, null, (spotsData, mapView) => {
      this.loadDataCallback(spotsData, mapView);
      if (typeof callback === 'function') {
        callback(spotsData);
      }
    });
  }

  displayRegion(date, regionIsoCode, callback) {
    this.spots = [];
    this.loadData(date, null, null, regionIsoCode, null, null, (spotsData, mapView) => {
      this.loadDataCallback(spotsData, mapView);
      if (typeof callback === 'function') {
        callback(spotsData);
      }
    });
  }

  displayDepartment(date, departmentIsoCode, callback) {
    this.spots = [];
    this.loadData(date, null, departmentIsoCode, null, null, null, (spotsData, mapView) => {
      this.loadDataCallback(spotsData, mapView);
      if (typeof callback === 'function') {
        callback(spotsData);
      }
    });
  }

  getSpotForecast(startDate, endDate, spotUuid, callback) {
    this.loadData(startDate, endDate, null, null, null, spotUuid, (spotsData, mapView) => {
      const spot = spotsData.spots[0];

      callback(spot, mapView);
    });
  }

  redraw() {
    if (!this.lastData) {
      return;
    }
    this.domElement.style = 'width:100%';
    this.init();
    this.loadDataCallback(this.lastData, this.mapView);
  }

  loadData(minDate, maxDate, departmentIsoCode, regionIsoCode, countryIsoCode, spotUuid, callback) {
    const xhr = new XMLHttpRequest();

    if (departmentIsoCode) this.mapView = DEPARTMENT_VIEW;
    else if (regionIsoCode) this.mapView = REGION_VIEW;
    else this.mapView = COUNTRY_VIEW;

    xhr.addEventListener('load', () => {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        this.lastData = JSON.parse(xhr.responseText);
        callback(this.lastData, this.mapView);
      }
    });

    const params = {};
    if (minDate) params['spots.feeds.date[after]'] = formatDate(minDate);
    if (minDate) params['spots.dayFeeds.date[after]'] = formatDate(minDate);
    if (maxDate) params['spots.feeds.date[before]'] = formatDate(maxDate);
    if (maxDate) params['spots.dayFeeds.date[before]'] = formatDate(maxDate);
    if (departmentIsoCode) params['spots.department.isoCode'] = departmentIsoCode;
    if (regionIsoCode) params['spots.region.isoCode'] = regionIsoCode;
    if (countryIsoCode) params['spots.country.isoCode'] = countryIsoCode;
    if (spotUuid) params.spotUuid = spotUuid;

    xhr.open(
      'GET',
      `${
        this.api
      }/forecast/customers/me/theme?groups[]=forecast&groups[]=forecast-day&${formatParams(
        params,
      )}`,
      true,
    );
    xhr.send();
  }
}

export function createMapInstance(domElement, config) {
  if (!domElement || !config) {
    throw new Error(
      '[@sportrizer/report-map-js] error : you must provide a dom element as first argument and a config object for the second',
    );
  }

  return new Map(domElement, config);
}
