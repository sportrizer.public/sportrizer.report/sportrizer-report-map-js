import { Map, createMapInstance } from './map';

test('Can instanciate a map', () => {
  // Set up our document body
  document.body.innerHTML = '<div id="map"></div>';

  const map = createMapInstance(document.querySelector('#map'), {});
  expect(map).toBeInstanceOf(Map);
});
