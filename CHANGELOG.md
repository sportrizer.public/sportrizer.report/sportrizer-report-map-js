# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.4](https://gitlab.com/sportrizer/sportrizer.com/v2/api/compare/v1.0.3...v1.0.4) (2021-02-18)


### Bug Fixes

* removed empty line before wind gust text ([6738540](https://gitlab.com/sportrizer/sportrizer.com/v2/api/commit/67385400983c44a1ed5926084a19c128fcc96d65))

### [1.0.3](https://gitlab.com/sportrizer/sportrizer.com/v2/api/compare/v1.0.2...v1.0.3) (2021-02-18)


### Bug Fixes

* default hasAqData empty callback ([96bd1fb](https://gitlab.com/sportrizer/sportrizer.com/v2/api/commit/96bd1fbf984daeac540ed5c50b91738499514fd3))

### [1.0.2](https://gitlab.com/sportrizer/sportrizer.com/v2/api/compare/v1.0.1...v1.0.2) (2021-02-18)


### Bug Fixes

* fixed error when wind speed is null => default to 0 ([4ebfcc4](https://gitlab.com/sportrizer/sportrizer.com/v2/api/commit/4ebfcc4f822a95f56998701d28675e941db305d9))

### [1.0.1](https://gitlab.com/sportrizer/sportrizer.com/v2/api/compare/v1.0.0...v1.0.1) (2021-02-18)


### Bug Fixes

* error when wind/gust null & hide icons when 0 or null ([e887a2e](https://gitlab.com/sportrizer/sportrizer.com/v2/api/commit/e887a2e53a0cac4c806224307252957541d2cd51))

## [1.0.0](https://gitlab.com/sportrizer/sportrizer.com/v2/api/compare/v0.2.2...v1.0.0) (2021-02-17)


### Features

* requesting day feeds ([ad32c02](https://gitlab.com/sportrizer/sportrizer.com/v2/api/commit/ad32c025a30ed948986213be787c4cea6a8ea9b4))


### Bug Fixes

* Fixed redraw method ([65eb69b](https://gitlab.com/sportrizer/sportrizer.com/v2/api/commit/65eb69bacda0ebbfa25e8bc477aff79ffaf134ce))
* **deps:** updated packages ([a9d4c00](https://gitlab.com/sportrizer/sportrizer.com/v2/api/commit/a9d4c0074808a1fc2a0f8f5787932ff8ab9130cc))


### Refactoring

* BREAKING CHANGES => Map should now be retrieved by an instanciator function "createMapInstance" ([b70827b](https://gitlab.com/sportrizer/sportrizer.com/v2/api/commit/b70827b4d4432124ddf85b79a6cbbd797e27c9d3))


### Documentation

* **CHANGELOG:** added .versionrc file for standard-version config ([60fbe52](https://gitlab.com/sportrizer/sportrizer.com/v2/api/commit/60fbe5234d4b73cb8b44c0f2263781fca239e919))
* Added changelog (standard-version) ([adbdd07](https://gitlab.com/sportrizer/sportrizer.com/v2/api/commit/adbdd07a320e77af7c794c0d75cfec14fa50d5db))
