#!/usr/bin/env bash
# Syntax: update-esdoc.sh REPOSITORY_SLUG

if [ "$#" -ne 1 ]
then
  echo '✗ Syntax update-esdoc.sh REPOSITORY_SLUG' >&2;
  exit 1;
fi

git=$1
git_url="git@gitlab.com:$git.git"
finish_path="https://doc.esdoc.org/github.com/$git/.finish.json"

echo "Starting ESDoc update: $git_url"

# Send documentation update request
printf 'Sending documentation update request... '
if curl --silent --show-error --fail 'https://doc.esdoc.org/api/create' -X POST --data-urlencode "gitUrl=$git_url" -o /dev/null
then
  printf '✓\n'
else
  printf ' ✗ Failed to send documentation update request\n' >&2;
  exit 1;
fi

# Wait for documentation to be updated
printf 'Waiting for documentation to be ready'
wait=30
for (( i=0; $i < $wait ; ++i )); do
  printf '.'
  sleep 1
  status=$(curl -Ss -o /dev/null -w '%{http_code}\n' "$finish_path")
  if [ "$status" -eq 200 ]
  then
    break;
  fi
done

if [ $i = $wait ]
then
  printf " ✗ Documentation not ready after $i seconds\n" >&2;
  exit 1;
else
  printf ' ✓\n'
fi